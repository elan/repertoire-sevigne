<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

    <xsl:template name="publication_edition_source">
        <H3>Publication</H3>
        <p> L'édition est publiée par
            <span title="tei:publisher">
                <xsl:value-of
                    select="normalize-space(tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:publisher)"
                />
            </span>
            <xsl:call-template name="add_endingPeriod_ifNotPresent"/>
        </p>

        <H3>Diffusion</H3>
        <p title="tei:availability">
            <xsl:text>Les données sont disponibles sous licence</xsl:text>
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="tei:licence/@target"/>
                </xsl:attribute>
                <xsl:value-of
                    select="normalize-space(tei:teiHeader/tei:fileDesc/tei:publicationStmt/tei:availability/tei:p)"
                />
            </a>
            <xsl:call-template name="add_endingPeriod_ifNotPresent"/>
        </p>

        <H3>Sources</H3>
        <xsl:for-each select="tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:p">
            <p>
                <xsl:apply-templates/>
                <xsl:call-template name="add_endingPeriod_ifNotPresent"/>
            </p>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="people_involved">
        <H3>Soutiens</H3>
        <div class="row border-bottom ml-3">
            <xsl:for-each select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:funder">
                <xsl:sort select="@n" order="ascending"/>
                <div class="m-1 mr-3" style="height: 100px;">
                    <a target="_blank">
                        <xsl:attribute name="href">
                            <xsl:value-of select="tei:link/@target"/>
                        </xsl:attribute>
                        <img class="h-50 d-inline-block">
                            <xsl:attribute name="src">
                                <xsl:value-of select="tei:figure/tei:graphic/@url"/>
                            </xsl:attribute>
                            <xsl:attribute name="title">
                                <xsl:value-of select="tei:title"/>
                            </xsl:attribute>
                        </img>
                    </a>
                </div>

            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template name="credits_figure">
        <H3>Crédits photographiques</H3>
        <div class="row">
            <div class="col-11 offset-1">
                <xsl:for-each-group select="//tei:figure/*[@type = 'rights']" group-by=".">
                    <xsl:sort order="descending" select="count(current-group())"/>
                    <li>
                        <xsl:value-of select="."/>
                        <ul>
                            <xsl:for-each select="current-group()">
                                <xsl:sort select="../tei:head" order="ascending" lang="fr"/>
                                <li>
                                    <img width="30px" style="inline">
                                        <xsl:attribute name="src">
                                            <xsl:value-of select="../tei:graphic/@url"/>
                                        </xsl:attribute>
                                    </img>
                                    <i>
                                        <xsl:value-of select="../tei:head"/>
                                    </i>
                                    <xsl:if test="exists(../tei:figDesc/tei:desc)">
                                        <xsl:text>, </xsl:text>
                                        <xsl:value-of select="../tei:figDesc/tei:desc"/>
                                    </xsl:if>
                                    <xsl:text>.</xsl:text>
                                    <xsl:if test="exists(../tei:graphic/@facs)">
                                        <xsl:text> Source : </xsl:text>
                                        <a>
                                            <xsl:attribute name="href">
                                                <xsl:value-of select="../tei:graphic/@facs"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="../tei:graphic/@facs"/>
                                        </a>
                                    </xsl:if>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </li>
                </xsl:for-each-group>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="add_endingPeriod_ifNotPresent">
        <xsl:if test="not(substring(., string-length(.)) = '.')">
            <xsl:text>.</xsl:text>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
