<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

    <xsl:variable name="br">
        <xsl:text>
</xsl:text>
    </xsl:variable>
    
    <xsl:template name="block_logos">
        <xsl:for-each select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:funder">
            <xsl:sort select="@n" order="ascending"/>
            <a target="_blank">
                <xsl:attribute name="href">
                    <xsl:value-of select="tei:link/@target"/>
                </xsl:attribute>
                <img>
                    <xsl:attribute name="src">
                        <xsl:value-of select="tei:figure/tei:graphic/@url"/>
                    </xsl:attribute>
                    <xsl:attribute name="title">
                        <xsl:value-of select="tei:title"/>
                    </xsl:attribute>
                </img>
            </a>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="block_sourcelink">
        <xsl:value-of select="//tei:link[@type = 'source']/@target"/>
    </xsl:template>

    <xsl:template name="block_menu">
        <xsl:for-each select="tei:TEI">
            <xsl:message> navbar-nav: <xsl:value-of select="@xml:id"/></xsl:message>
            <li class="nav-item">
                <a class="nav-link">
                    <xsl:attribute name="href">
                        <xsl:text>index.php?page=</xsl:text>
                        <xsl:value-of select="@xml:id"/>
                    </xsl:attribute>
                    <i>
                        <xsl:attribute name="class">
                            <xsl:text>fa </xsl:text>
                            <xsl:value-of
                                select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/@style"/>
                        </xsl:attribute>
                        <xsl:attribute name="aria-hidden">true</xsl:attribute>
                    </i>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                </a>
            </li>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
