module.exports = function(grunt) {

grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    copy: {
      ibraries: {
        files: [{
          expand: true,
          cwd: 'vendor/components/font-awesome/fonts/',
          src: ['**/*'],
          dest: 'css/fonts/'
        }]
      }
    },

    concat: {
      js: {
        src: [
          'vendor/components/jquery/jquery.min.js',
          'vendor/twbs/bootstrap/dist/js/bootstrap.min.js',
          'node_modules/clipboard/dist/clipboard.min.js',
          'js/*.js',
        ],
        dest: 'js/build/production.js'
      },
      css: {
        src: [
          'vendor/twbs/bootstrap/dist/css/bootstrap.min.css',
          'vendor/components/font-awesome/css/font-awesome.min.css',
          'css/style.css'
        ],
        dest: 'css/build/production.css'
      }
    },

    uglify: {
      js: {
        src: 'js/build/production.js',
        dest: 'js/build/production.min.js'
      }
    },

    cssmin: {
      target: {
        files: {
          'css/build/production.min.css': ['css/build/production.css']
        }
      }
    }



    });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.registerTask('default', ['copy', 'concat', 'uglify', 'cssmin']);
};
