# Installation
```
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:elan/repertoire-sevigne.git
cd madame-de-sevigne
composer install
npm install
npm run build
```

# Mise à jour
```
git pull origin master
composer install
npm install
npm run build
# Si nécessaire, regénération des contenus des pages
cd data/
saxonb-xslt -ext:on -xi tei/HN_Sevigne_corpus.xml xslt/sevigne.xsl > sevigne_corpus.log
```


# Données
Les données du projets sont dans le dossier data/ et sont organisées comme suit :
  * `tei/` : sources XML-TEI du site web
  * `img/` : images
  * `doc/` : autres fichiers
  * `xslt/` : feuilles de transformation XSL pour générer les contenus HTML du site

# `views/` : contenus de base et contenus générés
Certains contenus (fichiers) sont générés à partir des fichiers TEI à l'aide du XSLT.
D'autres permettent de gérer à la structure du site web.
Dans le dossier `views/` seul `base.html.twig` n'est pas généré par le XSLT. Son contenu ne dépend pas des sources TEI.
```bash
+-- views/
|   +-- base.html.twig # fichier de base permettant de générer grâce à Twig le squelette du site. À modifier le cas échéant.
|   +-- project.html.twig # fichier spécifique au projet généré par le XSLT permettant de spécifier certains contenus propre au projet et commun à toutes les pages du site (par exemple, le menu principal)
|   +-- XXX.html.twig # fichiers générés par le XSLT à partir des sources TEI
```

# `css` et styles
Le style utilise les bibliothèques Bootstrap et Fontawesome (voir les versions dans le fichier `composer.json`).
Des styles personalisés sont définis dans le fichier `style.css` du dossier `css/`.

# XSLT
Il prend en entrée un TEICorpus. Certaines informations génériques du site sont issues du TEIHeader de celui-ci (les TEIHeader des TEI inclut dans le corpus sont en revanche ignorés), notamment :
* le titre du site (balise `title` du html/head)
* le menu est élaboré à partir de la liste des TEI du corpus
* la liste des logos du bandau de bas de page est élaborées à partir de la liste des tei:funder
* la page "Crédits et mentions légales" utilise des informations issues de `titleStmt`
* [en cours] la page "À propos" utilise des informations issues de `editionStmt`, `publicationStmt` et `sourceDesc`

Pour chaque `TEI` du corpus, une page du site est générée :
* son titre est composé du titre du corpus (`tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title` du `teiCorpus`) et du titre du fichier TEI lui-même (`tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title`)
* seul le tei:body est traité
* images :
  * si plusieurs `tei:figure` sont déclarées, elles seront présentées sous-forme d'un carousel à droite du texte (voir la documentation de bootstrap)
  * si une seule est présentée, elle s'affichera à droite du texte
* sous-pages (ou onglet)
  * si le texte présente des divisions de type section (`tei:div[@type = 'section']`), une barre d'onglet s'affichera et proposra un sous-menu pour accéder aux différents contenus des sections déclarées.
* tous les `tei:XXX` sont transformés en l'élément HTML `<YYY/>` selon les correspondances suivantes :
  * `head` => `h2`
  * `p` => `p` avec ajout d'un point final si absent
  * `list` => `ul` sauf si `@type` prend pour une valeur spécifique
    * `item` => `li` avec la `class='text-ZZZ'` où ZZZ est la valeur de l'attribut `@rendition` du `tei:item`
  * `list[@type = 'academic']` => liste introduite par le contenu de `tei:head` (en `h3`) et une icône personalisée (fa-bank)
    * `item` => `li` contenant la valeur de `tei:label` puis une liste avec les `<li>` suivants et dans cet ordre :
      * `tei:desc`, `tei:ref`
  * `list[@type = 'places']` => liste introduite par le contenu de `tei:head` (en `h3`) et une icône personalisée (fa-map-marker)
    * `item` => `li` contenant la valeur de `tei:label` puis une liste avec les `<li>` suivants et dans cet ordre :
      * `tei:desc`, `tei:ref`
  * `list[@type = 'gallery']` => affiche une galerie d'image à partir des noeud `tei:figure` de la liste
    * `listBibl` => `ol` avec `class='bibl'` classée par ordre alphabétique du nom du premier auteur
      * `bibl` => `li`
      * `bibl[@type = 'cat']` => `li` introduit par la mention "Catalogue(s)" (le pluriel dépend du nombre de `tei:ref` dans ce `tei:bibl`)
      * `bibl[@type = 'dig']` => `li` introduit par la mention "Version(s)" (idem)
      * `bibl[@type = 'link']` => `li` introduit par la mention "Lien" (idem)
      * `bibl[@type = 'biblOfBibl']` => `li` introduit par la mention "Référence(s)" (idem)
  * `listPerson` : `div` avec `class='col'` et :
    * une alternance photo à gauche / texte à droite vs. photo à droite / texte à gauche
  *  `surname` => `span` avec `class="smallcaps"` et dont le contenu est capitalisé
  * `forename` => est capitalisé
  * `abbr` => `mark`
  * `title[[@level = 'm']` => `i`
  * `table` => `table`
    * `row[@role = 'label']` génère un `thead` et des `<th scope="col">` et sont traités avant les autres tei:row. Il est donc préférable de n'en déclarer qu'un seul
    * autres `row` et `cell` => `tr` et `td` au sein d'un `tbody`
  * `hi[@rend = 'italics']` => `i`
    * les autres sont laissés tels quels [à améliorer]
  * `listEvent` => `div` avec `class="col"` et un affichage des tei:even par ordre de date issue de l'un des attributs `@when` ou `@from` (attention à ne pas utiliser les deux concurentiellement !)
  * `event` => génère un affichage spécifique
    * `@facs` est utilisé pour afficher une image réprésentative de l'événementun
    * `tei:label` est affiché sous l'image
    * `tei:desc/tei:placeName`, `tei:desc/tei:orgName` et `tei:desc/tei:date` sont utilisés de façon spécifiques en haut du cadre décrivant l'événement
    * `tei:listBibl` est précédé de la mention "Bibliographie associée :"
  * `div` => `div` avec `@xml:id` qui devient l'attribut html `id`
  * `div[@type = 'section']` génère une `div` ayant les classes bootstrap nécessaires pour être manipulée comme sous-page (onglet)
